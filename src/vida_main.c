#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <util.h>
#include <vida.h>

int main (int argc, char **argv){
    
    char opcion;
    int filas=0;
    int columnas=0;
    int generaciones=0;
    int gen_tiempo=0;
    int celulas_init=0;

    while ((opcion = getopt (argc, argv, "f:c:g:s:i:"))!=-1)
    {
        switch(opcion)
        {
            case 'f':
                filas=atoi(optarg);
                printf("unknow option: %d\n", optopt);
            break;
            
            case 'c':
                columnas=atoi(optarg);
            break;
            
            case 'g':
                generaciones=atoi(optarg);
            break;
            
            case 's':
                gen_tiempo=atoi(optarg);
            break;

            case 'i':
                celulas_init=atoi(optarg);
            break;
            
            case '?':
                printf("unknow option: %c\n", optopt);
            break;
        }
    }
    
    //dibujar_grilla();
    //llenar_matriz_azar();

    return 0;
}