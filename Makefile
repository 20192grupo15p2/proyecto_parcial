./bin/vida: ./obj/vida_main.o ./obj/util.o
	gcc ./obj/vida_main.o ./obj/util.o -o ./bin/vida.o


./obj/vida_main.o: ./src/vida_main.c
	gcc -Wall -c -I./include/ ./src/vida_main.c -o ./obj/vida_main.o


./obj/util.o: ./src/util.c
	gcc -Wall -c -I./include/ ./src/util.c -o ./obj/util.o


.PHONY: clean
clean:
	rm ./bin/* ./obj/*.o